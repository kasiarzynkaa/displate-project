package config;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static config.WebDriverSingleton.getInstance;
import static config.WebDriverSingleton.quit;

public class TestConfig {

    public WebDriver driver;
    private static final String MAIN_URL = "https://displate.com/";

    @Before
    public void setUp() {
        driver = getInstance();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(MAIN_URL);
    }

    @After
    public void tearDown() {
        quit();
    }
}