package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPage extends BasePage {

    public CartPage() {
        super();
    }

    @FindBy(id = "select-country")
    private WebElement dropdown;

    @FindBy(xpath = "//div[@class=\"col-md-12\"]//div[@class=\"text text--center text--bold discount__toggle\"]")
    private WebElement discountButton;

    @FindBy(xpath = "//div[@class=\"col-xs-3\"]/div/span[2]")
    private WebElement totalPrice;

    @FindBy(xpath = "//div[@class=\"simplebar-content\"]/div[@value=\"US\"]")
    private WebElement country;

    @FindBy(xpath = "//span[@class=\"black-friday-text-regular\"]//strong[1]")
    private WebElement discount;

    @FindBy(xpath = "//span[@class=\"black-friday-text-regular\"]//strong[3]")
    private WebElement discountCode;

    @FindBy(xpath = "//input[@id=\"discount-code\"]")
    private WebElement discountField;

    @FindBy(xpath = "//input[@id=\"discount-apply\"]")
    private WebElement applyDiscountButton;

    @FindBy(xpath = "//div[@class=\"desk-cart-subtotal\"]/span[3]")
    private WebElement extraPrice;

    // get total price of poster
    public double getTotalPrice() {
        String total = totalPrice.getText();
        String digits = total.replaceAll("[^0-9.]", "");
        return Double.parseDouble(digits);
    }

    // click on dropdown and select shipping country
    public void selectShippingCountry(WebDriver driver) {
        dropdown.click();
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", country);
        driver.navigate().refresh();
    }

    // get poster price after country update
    public double getNewPosterPrice() {
        String priceIncludedCountry = totalPrice.getText();
        String digits = priceIncludedCountry.replaceAll("[^0-9.]", "");
        return Double.parseDouble(digits);
    }

    // get discount code and fill discount input
    public void setDiscount() {
        discountButton.click();
        String code = discountCode.getText();
        discountField.sendKeys(code);
        applyDiscountButton.click();
    }

    // get price after discount
    public double getDiscountPrice() {
        String discountPrice = extraPrice.getText();
        String digits = discountPrice.replaceAll("[^0-9.]", "");
        return Double.parseDouble(digits);
    }

    // get percentage discount value
    public double getDiscountValue() {
        String percentage = discount.getText();
        String digits = percentage.replaceAll("[^0-9.]", "");
        return Double.parseDouble(digits);
    }

    // calculate price after percentage discount
    public double calculateDiscountPrice() {
        double priceBeforeDiscount = getNewPosterPrice();
        double discountValue = getDiscountValue();
        double calculateResult = priceBeforeDiscount * (100 - discountValue) * 0.01;
        return calculateResult;
    }
}