package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ConfirmPage extends BasePage {

    public ConfirmPage() {
        super();
    }

    @FindBy(xpath = "//div[@class=\"col-md-12 text-center\"]/a[@href=\"/cart\"]")
    private WebElement proceedButton;

    // click proceed button and go to cart page
    public CartPage openCartPage() {
        proceedButton.click();

        return new CartPage();
    }
}