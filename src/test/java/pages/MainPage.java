package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Random;

public class MainPage extends BasePage {

    public MainPage() {
        super();
    }

    @FindBy(css = ".slick-active")
    private List<WebElement> products;

    // click on random active product and go to product page
    public ProductPage openProductPage() {
        products.get(new Random().nextInt(products.size())).click(); //

        return new ProductPage();
    }
}