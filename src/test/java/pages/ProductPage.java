package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Random;

public class ProductPage extends BasePage {

    public ProductPage() {
        super();
    }

    @FindBy(css = "#add-to-cart")
    private WebElement submitButton;

    @FindBy(css = ".js-item-price")
    private WebElement price;

    @FindBy(xpath = "//div/a[@class='link link--blue js-show-frames']")
    private WebElement selectButton;

    @FindBy(xpath = "//div[@class=\"customize-displate my--20 pt--20 customize-displate--active\"]//span[@class=\"input-radio__label\"]")
    private List<WebElement> finish;

    @FindBy(xpath = "//div[@class=\"customize-displate__row\"]/div[@class=\"tooltip tooltip--bottom-center tooltip--unset-height\"]")
    private List<WebElement> frame;

    // select randomly finish and frame for poster
    public void selectDetails() {
        selectButton.click();
        finish.get(new Random().nextInt(finish.size())).click();
        frame.get(new Random().nextInt(frame.size())).click();
    }

    // get poster total price
    public double getPosterPrice() {
        String posterPrice = price.getText();
        return Double.parseDouble(posterPrice);
    }

    // click submit and go to confirm page
    public ConfirmPage openConfirmPage() {
        submitButton.click();

        return new ConfirmPage();
    }
}