package tests;

import config.TestConfig;
import org.junit.Test;
import pages.CartPage;
import pages.ConfirmPage;
import pages.MainPage;
import pages.ProductPage;

public class AddProductToCardTest extends TestConfig {

    @Test
    public void getDiscountPriceOfProduct() {

        MainPage mainPage = new MainPage();
        ProductPage productPage = mainPage.openProductPage();

        productPage.selectDetails();
        double expectedPosterPrice = productPage.getPosterPrice();
        ConfirmPage confirmPage = productPage.openConfirmPage();

        CartPage cartPage = confirmPage.openCartPage();
        double cartPosterPrice = cartPage.getTotalPrice();
        cartPage.selectShippingCountry(driver);
        cartPage.getNewPosterPrice();
        cartPage.setDiscount();
        double priceAfterDiscount = cartPage.getDiscountPrice();
        double calculatedDiscountedPrice = cartPage.calculateDiscountPrice();

        // assert statements
        assert expectedPosterPrice == cartPosterPrice;
        assert priceAfterDiscount == calculatedDiscountedPrice;
    }
}